import java.util.Random;

public class RandomArray {
	private int size;
	private int numArray[];
	
	public RandomArray(int size) {
	this.size=size;
		
	}
	public int[] createRandomArray() {
		numArray[]=new int[size];
		 Random rand=new Random();
		 for(int i=0; i<size; i++) {
		int num=rand.nextInt(2);
		numArray[i]=num;
		}
		
		return numArray;
		
		
	}
	public void printArray(int printArray[]) {
		for(int i=0; i<printArray.length; i++) {
			System.out.print(printArray[i] + "| ");
		}
	}
}
